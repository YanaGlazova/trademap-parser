import sys
import html5lib 
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
import pandas as pd
from bs4 import BeautifulSoup as bs
import time
import logging
import random
import copy
import numpy as np
from tqdm import tqdm



class TradeSpider(object):

    def __init__(self):
        logging.info("start TradeSpider!")
        self.url = "https://www.trademap.org/Index.aspx"
        self.direction = None
        self.country = None
        self.by_thing = None
        self.cluster_size = None
        self.partner = None

    def setDriver(self, url=None):
        self.driver = webdriver.Chrome(executable_path="E:\\ChromeDriver\\chromedriver.exe")
        #self.driver = webdriver.Firefox(executable_path="E:\\Python\\geckodriver\\geckodriver.exe")
        options = Options() 
        self.driver.get(self.url)
        wait = WebDriverWait(self.driver, 10)

    # ---login----
    def login(self, ac, pw):
        self.ac = ac
        self.pw = pw
        wait = WebDriverWait(self.driver, 10)
        wait.until(
            EC.element_to_be_clickable(
                (By.XPATH, '//*[@id="ctl00_MenuControl_Label_Login"]'))).click()
        # Switch  iframe
#         wait.until(
#             EC.frame_to_be_available_and_switch_to_it('iframe_login'))  
        time.sleep(3)
        wait.until(
            EC.element_to_be_clickable(
                (By.XPATH, '//*[@id="Username"]'))).send_keys(ac)
        time.sleep(3)
        # Password
        wait.until(
            EC.element_to_be_clickable(
                (By.XPATH,
                 '//*[@id="Password"]'))).send_keys(pw)
        # Click
        time.sleep(5)
        self.driver.find_element('xpath',
#             '//*[@id="PageContent_Login1_Button"]').click()
            '/html/body/div[3]/div/div[2]/div/div/div/form/fieldset/div[4]/div/button').click()

        '''# Check
        soup = bs(self.driver.page_source, "lxml")
        if "Trade Map" in soup.title.text:
            logging.info("ITC login sucess!")
        else:
            logging.error("login faild")'''
      
    def selectExport(self):
        
        wait = WebDriverWait(self.driver, 5) 
        wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="ctl00_NavigationControl_DropDownList_TradeType"]')))
        select = Select(self.driver.find_element(By.XPATH, '//*[@id="ctl00_NavigationControl_DropDownList_TradeType"]'))
        select.select_by_visible_text('Exports')
    
    def def_page(self):
        self.driver.find_element('xpath',
#             '//*[@id="PageContent_Login1_Button"]').click()
            '/html/body/div[3]/div/div[2]/div/div/div/form/fieldset/div[4]/div/button').click()



    # Records
    def setRecords(self, n):
        '''
        Records = ["Exports", "Imports"]
        option = [1,2]
        '''
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.element_to_be_clickable((By.XPATH, f"//*[@id='ctl00_PageContent_label_RadioButton_TradeType_{str(n)}']"
            ))).click()

    # Indicators
    def setIndicators(self, n):
        '''
        Indicators = ["Values", "Quantities"]
        option = [1,2]
        '''
        xpath = '/html/body/form/div[3]/div[5]/table/tbody/tr[4]/td[2]/div/table/tbody/tr/td[3]/div[4]/select/option[{}]'.format(
            str(n))
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.element_to_be_clickable((By.XPATH, xpath))).click()
        Indicators = ["Values"]#, "Quantities"]
        logging.debug("setIndicators " + Indicators[n-1])
        
    # Timeseries
    def setTimePage(self, time_period=20, rows=25):
        

        row_option = {
            300: '300 per page',
            200: '200 per page',
            100: '100 per page',
            50: '50 per page',
            25: 'Default (25 per page)'
        }
       
          #Rows per page = "300 per page"
        wait = WebDriverWait(self.driver, 5) 
        wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="ctl00_PageContent_GridViewPanelControl_DropDownList_PageSize"]')))
        select = Select(self.driver.find_element(By.XPATH, 
                                                 '//*[@id="ctl00_PageContent_GridViewPanelControl_DropDownList_PageSize"]'))
        select.select_by_visible_text(row_option[rows])

    
    
    def setCountry(self):
        '''
        cluster_size: (int) 0 - 2 digit, 1 - 4 digit, 2 - 6 digit (Need auth to account. Use .login method)
        '''
        
        wait = WebDriverWait(self.driver, 5) 
        wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="ctl00_NavigationControl_DropDownList_Country"]')))
        select = Select(self.driver.find_element(By.XPATH, '//*[@id="ctl00_NavigationControl_DropDownList_Country"]'))
        select.select_by_visible_text(self.country)
        #logging.info(f"{cluster}")
        
        
    #Country
    def selectCountry(self, country=None):
        self.country = country
#         wait = WebDriverWait(self, 10)
        wait = WebDriverWait(self.driver, 5)
#         wait.until(
#             EC.element_to_be_clickable((
#             By.XPATH, 
#             '//*[@id="ctl00_NavigationControl_DropDownList_Country"]'))
#         ).click()
        wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@id='ctl00_PageContent_RadComboBox_Country']"))).click()
        tap = wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@id='ctl00_PageContent_RadComboBox_Country_Input']")))
        tap.send_keys(self.country)
        wait.until(EC.element_to_be_clickable((By.XPATH,f"//*[@title='{self.country}']"))).click()

        
    #select partner
    def selectPartner(self, partner):
        self.partner = partner
        wait = WebDriverWait(self.driver, 5)
        select = Select(self.driver.find_element(By.XPATH,
                                                '//*[@id="ctl00_NavigationControl_DropDownList_Partner"]'))
        select.select_by_visible_text(self.partner) 
        logging.info(f"Select partner : {self.partner}")
 
    
    def trade_type(self, go = 'companies'):
        wait = WebDriverWait(self.driver, 5)
        if go == 'companies':
            WebDriverWait(self.driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='ctl00_PageContent_Button_Companies']"
            ))).click()
    
    def toStartPage(self):
        self.driver.get(self.url)
        
    # Products
    def selectProducts(self, n):
        '''
        n: 選取 Products的第n個options
        '''
        self.product = str(n)
        wait = WebDriverWait(self.driver, 3)
        wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@id='ctl00_PageContent_RadComboBox_Product']"))).click()
        tap1 = wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@id='ctl00_PageContent_RadComboBox_Product_Input']")))
        tap1.clear()
        time.sleep(3)
        tap1.send_keys(self.product)
        tap1.click()
        wait.until(EC.element_to_be_clickable((By.XPATH, f"//div[starts-with(@title, '{self.product}')]"))).click()
        

    def save(self, filename):
        table_source = self.driver.find_element_by_xpath(
            '/html/body/form/div[3]/table/tbody/tr[3]/td').get_attribute('innerHTML')
        df = pd.read_html(table_source)
        # print(df.head)

        df.to_pickle(filename+'.pickle')
        logging.info("save {}.pickle".format(filename))

    def showdf(self, returned=False):
        '''
        Show df from page (recommended 300 rows per page). Option returned: (bool) return df
        '''
        table_source = self.driver.find_element(By.XPATH,
            '/html/body/form/div[3]/table/tbody/tr[3]/td').get_attribute(
                'innerHTML')
        df = pd.read_html(table_source)
        print(df)
        if returned:
            return df
        
    def check_page(self):
        time.sleep(random.randint(2, 7))
        self.setTimePage(rows=300)
        table=self.driver.find_element(By.XPATH,'//*[@id="ctl00_PageContent_MyGridView1"]')
        tbody = table.find_element(By.TAG_NAME, "tbody")
        rows = tbody.find_elements(By.TAG_NAME, "tr")
        cols = rows[0].find_elements(By.TAG_NAME, "td")
        if len(cols) > 2:
            content = self.get_content([])
        else:
            links = self.find_links()
            time.sleep(random.randint(3,7))
            content = self.get_content(links)
        return content
    
    def find_links(self):
        time.sleep(random.randint(2, 7))
        self.setTimePage(rows=300)
        time.sleep(random.randint(2, 7))
        self.setCountry()
        time.sleep(random.randint(2, 7))
        self.selectExport()
        time.sleep(random.randint(2, 7))
        table=self.driver.find_element(By.XPATH,'//*[@id="ctl00_PageContent_MyGridView1"]')
        tbody = table.find_element(By.TAG_NAME, "tbody")
        rows = tbody.find_elements(By.TAG_NAME, "tr")
        links = [] 
        for row in rows[1:]:       
            col = row.find_elements(By.TAG_NAME, "td")[0] 
            links.append(col.text)
        return links
    
    def links_content(self, links):
        df = pd.DataFrame()
        for link in links: 
            try:
                wait = WebDriverWait(self.driver, 5)
                wait.until(EC.element_to_be_clickable((By.LINK_TEXT, link))).click()
                #self.driver.find_element(By.LINK_TEXT, link).click()
            except:
                print('link not found')
                continue
            self.goBack()
            time.sleep(random.randint(2, 7))
            self.setTimePage(rows=300)
            time.sleep(random.randint(2, 7))
            self.setCountry()
            time.sleep(random.randint(2, 7))
            self.selectExport()
            time.sleep(random.randint(2, 7))
            table = self.driver.find_element(By.XPATH,'//*[@id="ctl00_PageContent_MyGridView1"]')
            if table==None:
                return pd.DataFrame()
            tbody = table.find_element(By.TAG_NAME, "tbody")
            rows = tbody.find_elements(By.TAG_NAME, "tr")
            rows_list = []
            for row in rows:
                col_dict = {}
                cols = row.find_elements(By.TAG_NAME, "td")
                if len(cols) == 0:
                    continue
                if len(cols) < 7:
                    col_dict.update({6:None})
                for i, col in enumerate(cols):
                    col_dict.update({i:col.text})
                col_dict = {key:col_dict[key] for key in sorted(col_dict.keys())}

                rows_list.append(col_dict)
            df_temp = pd.DataFrame(rows_list)
            self.driver.find_element(By.XPATH, '//*[@id="ctl00_PageContent_GridViewPanelControl_Label_tabTable"]').click()
            time.sleep(random.randint(2, 7))
            self.driver.find_element(By.XPATH, '//*[@id="ctl00_PageContent_GridViewPanelControl_Label_tabCompanies"]').click()
            time.sleep(random.randint(2, 7))
            self.setTimePage(rows=300)
            time.sleep(random.randint(2, 7))
            self.setCountry()
            time.sleep(random.randint(2, 7))
            self.selectExport()
            time.sleep(random.randint(2, 7))
            df = pd.concat([df, df_temp])
        return df

        
    def get_content(self, links):
        time.sleep(3)
        df = pd.DataFrame()
        n = 20
        if len(links) > n:
            len_series = np.ceil(len(links) / n)
            for i in range(int(len_series)):
                self.toStartPage()
                self.goBack()
                time.sleep(5)
                self.selectCountry(self.country)
                self.selectProducts(self.product)
                self.trade_type("companies")
                time.sleep(random.randint(3, 7))
                try:
                    df_temp = self.links_content(links[n*i:n*(i+1)])
                except:
                    print(f'series {i} failed')
                    self.driver.get(self.url)
                    time.sleep(5)
                    self.selectCountry(self.country)
                    self.selectProducts(self.product)
                    self.trade_type("companies")
                    df_temp = self.links_content(links[n*i:n*(i+1)])
                df = pd.concat([df, df_temp])
            self.toStartPage()
            return df
        elif len(links) > 0:
            df = self.links_content(links)
            self.toStartPage()
            return df
        rows_list = []
        table=self.driver.find_element(By.XPATH,'//*[@id="ctl00_PageContent_MyGridView1"]')
        tbody = table.find_element(By.TAG_NAME, "tbody")
        rows = tbody.find_elements(By.TAG_NAME, "tr") 
        for row in rows[1:]: 
            col_dict = {}
            cols = row.find_elements(By.TAG_NAME, "td")
            if len(cols) == 0:
                        continue
            if len(cols) < 7:
                col_dict.update({6:None})
            for i, col in enumerate(cols):
                col_dict.update({i:col.text})
            rows_list.append(col_dict)
        df_temp = pd.DataFrame(rows_list)
        self.toStartPage()
        return df_temp
    
    def goBack(self):
        current_page = self.driver.current_url
        if "SilentLogin" in current_page:
            self.driver.execute_script('window.history.go(-1)')
        else:
            pass
    
    def scrapCompany(self, id_company):
        self.driver.execute_script(f'''window.open("https://www.trademap.org/CompanyPage.aspx?compid={id_company}&src=1","_blank");''')
        new_tab = self.driver.window_handles[-1]
        self.driver.switch_to.window(window_name=new_tab)
        time.sleep(5)
        tables = self.driver.find_elements(By.TAG_NAME, 'table')
        for t in tables:
            rows = t.find_elements(By.TAG_NAME, 'tr')
            for r in rows:
                columns = r.find_elements(by.TAG_NAME,'td')
                for col in columns:
                    print(col.text)
            self.driver.close()

    def popUpCheck(self):
        try:
            self.driver.find_element(By.XPATH, '//*[@id="ctl00_MenuControl_Button_ClosePopupRestriction"]').click()
            print('relogin')
            self.driver.find_element(By.XPATH, '//*[@id="ctl00_MenuControl_Label_Login"]').click()
            try:
                self.driver.find_element(By.XPATH, '/html/body/div[3]/div[@class="login-page"]')
                self.login(self.ac, self.pw)
            except:
                pass
        except:
            pass
        

    def clearCache(self):
        self.driver.delete_all_cookies()
        
        
    def close(self):
        self.driver.close()
        logging.info("Close Driver!!")


#test example

s = TradeSpider()
s.setDriver()
try:
    s.login(ac, pw)
except:
    s.login(ac, pw)
time.sleep(20)
# try:
#     s.def_page()
# except:
#     print('No capcha')
#     pass
#s.setTimePage()
#s.selectAgg()
#s.setCluster(2)
# spyder('Hong Kong, China', s)
for p in tqdm(products[600:700]):
    try:
        s.popUpCheck()
        failed = 1
        s.setRecords('Export')
        s.selectCountry('India')
        s.selectProducts(p)
        s.trade_type("companies")
        time.sleep(5)
        content = s.check_page()
        failed = 0
        content = content.dropna(how='all', axis=0)
        if len(content) < 1:
            content = pd.DataFrame({0:[], 1:[], 2:[], 3:[], 4:[], 5:[], 6:[]})
        content.columns = ['Company_name', 'Number of product or service categories traded', 'Number of employees', 'Turnover (USD)', 'Country', 'City', 'Website']
        content['HS'] = p
        content.to_csv(f"E:\\Python\\parser_data\\{p}.csv")
    except:
        print(f"2nd try for {p}")
        s.popUpCheck()
        s.toStartPage()
        s.selectCountry('India')
        s.selectProducts(p)
        s.trade_type("companies")
        time.sleep(5)
        content = s.check_page()
        failed = 0
        content = content.dropna(how='all', axis=0)
        if len(content) < 1:
            content = pd.DataFrame({0:[], 1:[], 2:[], 3:[], 4:[], 5:[], 6:[]})
        content.columns = ['Company_name', 'Number of product or service categories traded', 'Number of employees', 'Turnover (USD)', 'Country', 'City', 'Website']
        content['HS'] = p
        content.to_csv(f"E:\\Python\\parser_data\\{p}.csv")
    finally:
        if failed == 1:
            print(f"Code {p} is failed")
            failed_codes.append(p)
            continue
s.clearCache()
time.sleep(5)
s.close()